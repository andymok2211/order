module gitlab.com/andymok221/order

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-pg/pg v8.0.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.5.0
	github.com/google/uuid v1.1.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/tools/gopls v0.1.3 // indirect
	googlemaps.github.io/maps v0.0.0-20190709232500-0dbd631282e2
	mellium.im/sasl v0.2.1 // indirect
)
