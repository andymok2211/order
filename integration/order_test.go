package integration

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/andymok221/order/handler"
	"gitlab.com/andymok221/order/maps"
	"gitlab.com/andymok221/order/migration"
	"gitlab.com/andymok221/order/model"
	"gitlab.com/andymok221/order/repository"
	"gitlab.com/andymok221/order/route"
	"gitlab.com/andymok221/order/utils"
)

func TestIntegrationHandler(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Integration Suite")
}

var _ = Describe("Order Repository Integration", func() {
	var (
		r         *chi.Mux
		h         *handler.Handler
		db        *pg.DB
		gMap      maps.Map
		orderRepo repository.OrderRepository
		err       error
	)
	BeforeEach(func() {
		db = pg.Connect(&pg.Options{
			Addr:     fmt.Sprintf("%s:%s", os.Getenv("PG_HOST"), os.Getenv("PG_PORT")),
			User:     os.Getenv("PG_USER"),
			Password: os.Getenv("PG_PASSWORD"),
			Database: os.Getenv("PG_DB"),
		})
		migration.RunMigration("../migration")
		orderRepo = repository.NewOrderRepo(db)
		r = chi.NewRouter()
		r.Use(render.SetContentType(render.ContentTypeJSON))
		gMap, err = maps.NewGoogleMap(os.Getenv("API_KEY"))
		if err != nil {
			log.Fatalf("Cannot init google map:%s", err.Error())
		}
		h = handler.New(orderRepo, gMap)
		route.RegisterOrderRoute(r, h)
		db.Exec("DELETE FROM orders")
	})
	AfterSuite(func() {
		db.Exec("DELETE FROM orders")
	})
	Describe("Place order", func() {
		It("should return order with ID", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "POST", "/orders", nil, strings.NewReader(`
			{
				"origin": ["40.6655101","-73.89123"],
				"destination": ["40.6655101","-73.89"]
			}`))
			var order model.Order
			json.Unmarshal([]byte(body), &order)
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(order.ID).NotTo(BeEmpty())
			Expect(order.Status).To(Equal(model.StatusUnassigned))
		})
	})
	Describe("Take order", func() {
		var (
			order      *model.Order
			takenOrder *model.Order
		)
		BeforeEach(func() {
			order, _ = orderRepo.Create(&model.Order{Distance: 1, Status: model.StatusUnassigned})
			takenOrder, _ = orderRepo.Create(&model.Order{Distance: 1, Status: model.StatusTaken})
		})
		It("should take the order successfully", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "PATCH", fmt.Sprintf("/orders/%s", order.ID), nil, strings.NewReader(`
			{
				"status":"TAKEN"
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(body).To(Equal(`{"status":"SUCCESS"}`))
		})
		It("should only be updated successfully once if there's concurrent", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			bodies := make(chan string, 2)
			go func() {
				_, body := utils.TryRequest(ts, "PATCH", fmt.Sprintf("/orders/%s", order.ID), nil, strings.NewReader(`
				{
					"status":"TAKEN"
				}`))
				bodies <- body
			}()
			go func() {
				_, body := utils.TryRequest(ts, "PATCH", fmt.Sprintf("/orders/%s", order.ID), nil, strings.NewReader(`
				{
					"status":"TAKEN"
				}`))
				bodies <- body
			}()
			total := 0
			successCase := 0
			for body := range bodies {
				total++
				if strings.Index(body, "SUCCESS") >= 0 {
					successCase++
				}
				if total == 2 {
					break
				}
			}
			Expect(successCase).To(Equal(1))
		})
		It("should not update the order if the order has been taken", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "PATCH", fmt.Sprintf("/orders/%s", takenOrder.ID), nil, strings.NewReader(`
			{
				"status":"TAKEN"
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"Cannot take order"}`))
		})
	})
	Describe("List order", func() {
		BeforeEach(func() {
			for i := 0; i < 10; i++ {
				orderRepo.Create(&model.Order{Distance: 1, Status: model.StatusUnassigned})
			}
		})
		It("should get lists of order", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			var orders []*model.Order
			resp, body := utils.TryRequest(ts, "GET", "/orders", nil, nil)
			json.Unmarshal([]byte(body), &orders)
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(orders).To(HaveLen(10))
		})
		It("should get lists of order with correct limit", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			var orders []*model.Order
			resp, body := utils.TryRequest(ts, "GET", "/orders?limit=3", nil, nil)
			json.Unmarshal([]byte(body), &orders)
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(orders).To(HaveLen(3))
		})
		It("should return empty results if there is no result", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			var orders []*model.Order
			resp, body := utils.TryRequest(ts, "GET", "/orders?page=999&limit=3", nil, nil)
			json.Unmarshal([]byte(body), &orders)
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(orders).To(HaveLen(0))
		})
	})
})
