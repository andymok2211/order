package model

type OrderStatus string

const (
	StatusUnassigned OrderStatus = "UNASSIGNED"
	StatusTaken      OrderStatus = "TAKEN"
)

type Order struct {
	ID       string      `json:"id"`
	Distance int         `json:"distance"`
	Status   OrderStatus `json:"status"`
}
