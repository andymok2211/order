package repository

import (
	"fmt"

	"github.com/go-pg/pg"
	"gitlab.com/andymok221/order/model"
)

// OrderRepository is responsible for the database operation of order model
type OrderRepository interface {
	Create(order *model.Order) (*model.Order, error)
	Update(id string, update map[string]interface{}, condition map[string]interface{}) (*model.Order, error)
	Fetch(offset, limit int) ([]*model.Order, error)
}

// the implementation of OrderRepository in Postgres
type postgresOrder struct {
	db *pg.DB
}

func NewOrderRepo(db *pg.DB) OrderRepository {
	return &postgresOrder{
		db: db,
	}
}

func (o *postgresOrder) Create(order *model.Order) (*model.Order, error) {
	err := o.db.Insert(order)
	return order, err
}

func (o *postgresOrder) Update(id string, update map[string]interface{}, condition map[string]interface{}) (*model.Order, error) {
	order := &model.Order{}
	orderModel := o.db.Model(order)
	for field, value := range update {
		orderModel.Set(fmt.Sprintf("%s = ?", field), value)
	}
	for field, value := range condition {
		orderModel.Where(fmt.Sprintf("%s = ?", field), value)
	}
	_, err := orderModel.Where("id = ?", id).Returning("*").Update()
	return order, err
}

func (o *postgresOrder) Fetch(offset, limit int) ([]*model.Order, error) {
	orders := []*model.Order{}
	err := o.db.Model(&orders).Offset(offset).Limit(limit).Select(&orders)
	return orders, err
}
