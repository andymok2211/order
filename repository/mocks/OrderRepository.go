// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"
import model "gitlab.com/andymok221/order/model"

// OrderRepository is an autogenerated mock type for the OrderRepository type
type OrderRepository struct {
	mock.Mock
}

// Create provides a mock function with given fields: order
func (_m *OrderRepository) Create(order *model.Order) (*model.Order, error) {
	ret := _m.Called(order)

	var r0 *model.Order
	if rf, ok := ret.Get(0).(func(*model.Order) *model.Order); ok {
		r0 = rf(order)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*model.Order)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*model.Order) error); ok {
		r1 = rf(order)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Fetch provides a mock function with given fields: offset, limit
func (_m *OrderRepository) Fetch(offset int, limit int) ([]*model.Order, error) {
	ret := _m.Called(offset, limit)

	var r0 []*model.Order
	if rf, ok := ret.Get(0).(func(int, int) []*model.Order); ok {
		r0 = rf(offset, limit)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*model.Order)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int, int) error); ok {
		r1 = rf(offset, limit)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Update provides a mock function with given fields: id, update, condition
func (_m *OrderRepository) Update(id string, update map[string]interface{}, condition map[string]interface{}) (*model.Order, error) {
	ret := _m.Called(id, update, condition)

	var r0 *model.Order
	if rf, ok := ret.Get(0).(func(string, map[string]interface{}, map[string]interface{}) *model.Order); ok {
		r0 = rf(id, update, condition)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*model.Order)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, map[string]interface{}, map[string]interface{}) error); ok {
		r1 = rf(id, update, condition)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
