package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-pg/pg"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"gitlab.com/andymok221/order/handler"
	"gitlab.com/andymok221/order/maps"
	"gitlab.com/andymok221/order/migration"
	"gitlab.com/andymok221/order/repository"
	"gitlab.com/andymok221/order/route"
)

func connectDB() *pg.DB {
	return pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%s", os.Getenv("PG_HOST"), os.Getenv("PG_PORT")),
		User:     os.Getenv("PG_USER"),
		Password: os.Getenv("PG_PASSWORD"),
		Database: os.Getenv("PG_DB"),
	})
}

func main() {
	migration.RunMigration("migration")
	db := connectDB()
	defer db.Close()
	orderRepo := repository.NewOrderRepo(db)
	gmap, err := maps.NewGoogleMap(os.Getenv("API_KEY"))
	if err != nil {
		log.Panicf("Cannot init google map: %s", err.Error())
	}

	h := handler.New(orderRepo, gmap)
	r := route.New(h)
	srv := http.Server{Addr: fmt.Sprintf(":%s", os.Getenv("PORT")), Handler: r}
	listener, err := net.Listen("tcp", srv.Addr)
	if err != nil {
		log.Panicf("Failed to listen on port: %s, error: %s", os.Getenv("PORT"), err.Error())
	}

	go srv.Serve(listener)
	close := make(chan os.Signal, 1)
	signal.Notify(close, os.Interrupt)
	<-close
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Printf("Cannot gracefully shutdown: %s", err.Error())
	}
}
