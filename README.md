# Order RESTful API

## Requirements
- Docker installed
- Go v1.12

## Google Map API Key
Please put the Google map API key in `.env`, it is named as `API_KEY`.

## Startup
Use the `start.sh` script

## Testing
- Unit testing: `make test`
- Integration testing: `make integration-test`

## Explanation
- `handler`: Handler for different routes
- `integration`: Integration test
- `maps`: Implementation of map related functions
- `migration`: Contains database migration script
- `model`: Data model
- `repository`: Database operation logic with model
- `response`: Helper function for generating response
- `route`: Define routes
- `utils`: Some utility functions