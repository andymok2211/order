package response

import (
	"net/http"

	"github.com/go-chi/render"
)

// JSONResponse allows you to return arbitrary response in JSON
func JSONResponse(w http.ResponseWriter, r *http.Request, statusCode int, payload interface{}) {
	render.Status(r, statusCode)
	render.JSON(w, r, payload)
}

// ErrResponse allows you to return an error in JSON format
func ErrResponse(w http.ResponseWriter, r *http.Request, statusCode int, err error) {
	JSONResponse(w, r, statusCode, map[string]interface{}{"error": err.Error()})
}
