CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE orders (
    id uuid DEFAULT uuid_generate_v4(),
    distance integer,
    status varchar(16),
    PRIMARY KEY (id)
);