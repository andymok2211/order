package migration

import (
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func RunMigration(path string) {
	m, err := migrate.New(
		fmt.Sprintf("file://%s", path),
		fmt.Sprintf(
			"postgres://%s:%s/%s?user=%s&password=%s&sslmode=disable",
			os.Getenv("PG_HOST"),
			os.Getenv("PG_PORT"),
			os.Getenv("PG_DB"),
			os.Getenv("PG_USER"),
			os.Getenv("PG_PASSWORD"),
		),
	)
	if err != nil {
		log.Fatalf("Migration error: %s", err.Error())
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Fatalf("Migration error: %s", err.Error())
	}
}
