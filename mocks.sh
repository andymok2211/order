#!/bin/bash
: ${GOPATH:?"ERROR: GOPATH not set"}
export PATH=$PATH:$GOPATH/bin
mockery_path=$(which mockery)
if [ ! -x "$mockery_path" ]; then
	echo "ERROR: mockery not found. Please install by "
	echo "go get github.com/vektra/mockery/.../"
	exit 1
fi

mockery -dir=maps -all -output=maps/mocks
mockery -dir=repository -all -output=repository/mocks