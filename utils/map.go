package utils

import (
	"math"
	"strconv"
)

func ValidateCoordinate(values []string) bool {
	if len(values) != 2 {
		return false
	}

	if !validateLatitude(values[0]) || !validateLongitude(values[1]) {
		return false
	}

	return true
}

func validateLatitude(latitude string) bool {
	value, err := strconv.ParseFloat(latitude, 32)
	if err != nil {
		return false
	}

	return math.Abs(value) < 90
}

func validateLongitude(longitude string) bool {
	value, err := strconv.ParseFloat(longitude, 32)
	if err != nil {
		return false
	}

	return math.Abs(value) < 180
}
