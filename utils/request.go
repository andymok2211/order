package utils

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
)

// TryRequest is for testing HTTP call
func TryRequest(ts *httptest.Server, method, path string, header http.Header, body io.Reader) (*http.Response, string) {
	req, err := http.NewRequest(method, ts.URL+path, body)
	if err != nil {
		return nil, ""
	}

	for key, values := range header {
		req.Header.Set(key, strings.Join(values, " "))
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, ""
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, ""
	}
	defer resp.Body.Close()

	return resp, strings.TrimRight(string(respBody), "\n")
}
