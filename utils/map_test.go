package utils

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Map utils", func() {
	Describe("ValidateCoordinate", func() {
		It("should return false if there are not two values", func() {
			Expect(ValidateCoordinate([]string{"1"})).To(BeFalse())
		})
		It("should return false if it one of the value is not valid", func() {
			Expect(ValidateCoordinate([]string{"s", "1"})).To(BeFalse())
		})
		It("should return true if it is a valid coordinate", func() {
			Expect(ValidateCoordinate([]string{"1", "1"})).To(BeTrue())
		})
	})
	Describe("validateLatitude", func() {
		It("should return false if it is not a number", func() {
			Expect(validateLatitude("abc")).To(BeFalse())
		})
		It("should return false if it is out of range", func() {
			Expect(validateLatitude("180")).To(BeFalse())
		})
		It("should return true if it is valid", func() {
			Expect(validateLatitude("45")).To(BeTrue())
		})
	})
	Describe("validateLongitude", func() {
		It("should return false if it is not a number", func() {
			Expect(validateLongitude("abc")).To(BeFalse())
		})
		It("should return false if it is out of range", func() {
			Expect(validateLongitude("181")).To(BeFalse())
		})
		It("should return true if it is valid", func() {
			Expect(validateLongitude("45")).To(BeTrue())
		})
	})
})
