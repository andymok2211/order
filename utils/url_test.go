package utils

import (
	"net/http"
	"net/url"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestUtilsHandler(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Utils Suite")
}

var _ = Describe("URL utils", func() {
	Describe("IntQuery", func() {
		It("should return default value if no query string in URL", func() {
			uri, _ := url.ParseRequestURI("/")
			value, err := IntQuery(&http.Request{URL: uri}, "field", 0)
			Expect(err).To(BeNil())
			Expect(value).To(Equal(0))
		})
		It("should return the value from query string if specified in URL", func() {
			uri, _ := url.ParseRequestURI("/?field=10")
			value, err := IntQuery(&http.Request{URL: uri}, "field", 0)
			Expect(err).To(BeNil())
			Expect(value).To(Equal(10))
		})
		It("should return error if the query string is not integer", func() {
			uri, _ := url.ParseRequestURI("/?field=abc")
			_, err := IntQuery(&http.Request{URL: uri}, "field", 0)
			Expect(err).NotTo(BeNil())
		})
	})
})
