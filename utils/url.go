package utils

import (
	"net/http"
	"strconv"
)

func IntQuery(r *http.Request, field string, defaultValue int) (int, error) {
	str := r.URL.Query().Get(field)
	if str == "" {
		return defaultValue, nil
	}

	intVal, err := strconv.ParseInt(str, 10, 32)
	return int(intVal), err
}
