package handler_test // avoid import cycle

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-pg/pg"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/mock"
	"gitlab.com/andymok221/order/handler"
	mapsMocks "gitlab.com/andymok221/order/maps/mocks"
	"gitlab.com/andymok221/order/model"
	repoMocks "gitlab.com/andymok221/order/repository/mocks"
	"gitlab.com/andymok221/order/route"
	"gitlab.com/andymok221/order/utils"
)

func TestHandlerHandler(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Handler Suite")
}

var _ = Describe("Order handler", func() {
	var (
		r             *chi.Mux
		h             *handler.Handler
		orderRepoMock *repoMocks.OrderRepository
		mapMock       *mapsMocks.Map
	)
	Context("GET orders", func() {
		BeforeEach(func() {
			r = chi.NewRouter()
			r.Use(render.SetContentType(render.ContentTypeJSON))
			orderRepoMock = &repoMocks.OrderRepository{}
			mapMock = &mapsMocks.Map{}
			h = handler.New(orderRepoMock, mapMock)
			route.RegisterOrderRoute(r, h)
		})
		It("should return list of orders on valid request", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			orderRepoMock.On("Fetch", 0, 10).Return([]*model.Order{&model.Order{
				ID:       "test",
				Distance: 1,
				Status:   model.StatusUnassigned,
			}}, nil)
			resp, body := utils.TryRequest(ts, "GET", "/orders", nil, nil)
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(body).To(Equal(`[{"id":"test","distance":1,"status":"UNASSIGNED"}]`))
		})
		It("should return list of orders on valid request with valid page and limit", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			orderRepoMock.On("Fetch", 180, 20).Return([]*model.Order{&model.Order{
				ID:       "test",
				Distance: 1,
				Status:   model.StatusUnassigned,
			}}, nil)
			resp, body := utils.TryRequest(ts, "GET", "/orders?page=10&limit=20", nil, nil)
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(body).To(Equal(`[{"id":"test","distance":1,"status":"UNASSIGNED"}]`))
		})
		It("should return error if cannot fetch order", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			orderRepoMock.On("Fetch", 0, 10).Return(nil, errors.New("dummy error"))
			resp, body := utils.TryRequest(ts, "GET", "/orders", nil, nil)
			Expect(resp.StatusCode).To(Equal(http.StatusInternalServerError))
			Expect(body).To(Equal(`{"error":"dummy error"}`))
		})
		It("should return error on invalid limit", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "GET", "/orders?limit=abc", nil, nil)
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"limit query should be integer"}`))
		})
		It("should return error on invalid page", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "GET", "/orders?page=abc", nil, nil)
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"page query should be integer"}`))
		})
	})
	Context("POST order", func() {
		BeforeEach(func() {
			r = chi.NewRouter()
			r.Use(render.SetContentType(render.ContentTypeJSON))
			orderRepoMock = &repoMocks.OrderRepository{}
			mapMock = &mapsMocks.Map{}
			h = handler.New(orderRepoMock, mapMock)
			route.RegisterOrderRoute(r, h)
		})
		It("should return a order when placed successfully", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			mapMock.On("GetDistance", []string{"1", "2"}, []string{"3", "4"}).Return(20, nil)
			orderRepoMock.On("Create", &model.Order{Distance: 20, Status: model.StatusUnassigned}).Return(&model.Order{
				ID:       "test",
				Distance: 1,
				Status:   model.StatusUnassigned,
			}, nil)
			resp, body := utils.TryRequest(ts, "POST", "/orders", nil, strings.NewReader(`
			{
				"origin": ["1","2"],
				"destination": ["3","4"]	
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(body).To(Equal(`{"id":"test","distance":1,"status":"UNASSIGNED"}`))
		})
		It("should return error if origin is wrong", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "POST", "/orders", nil, strings.NewReader(`
			{
				"origin": ["abc"],
				"destination": ["3","4"]	
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"Invalid origin coordinate"}`))
		})
		It("should return error if destination is wrong", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "POST", "/orders", nil, strings.NewReader(`
			{
				"origin": ["1","2"],
				"destination": ["abc"]	
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"Invalid destination coordinate"}`))
		})
	})
	Context("PATCH order", func() {
		BeforeEach(func() {
			r = chi.NewRouter()
			r.Use(render.SetContentType(render.ContentTypeJSON))
			orderRepoMock = &repoMocks.OrderRepository{}
			mapMock = &mapsMocks.Map{}
			h = handler.New(orderRepoMock, mapMock)
			route.RegisterOrderRoute(r, h)
		})
		It("should return success if an order can be taken", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			orderRepoMock.On("Update", "test", mock.Anything, mock.Anything).Return(&model.Order{
				ID:       "test",
				Distance: 1,
				Status:   model.StatusTaken,
			}, nil)
			resp, body := utils.TryRequest(ts, "PATCH", "/orders/test", nil, strings.NewReader(`
			{
				"status":"TAKEN"
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusOK))
			Expect(body).To(Equal(`{"status":"SUCCESS"}`))
		})
		It("should return error if unable to take the order", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			orderRepoMock.On("Update", "test", mock.Anything, mock.Anything).Return(nil, pg.ErrNoRows)
			resp, body := utils.TryRequest(ts, "PATCH", "/orders/test", nil, strings.NewReader(`
			{
				"status":"TAKEN"
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"Cannot take order"}`))
		})
		It("should return error if it is invalid status", func() {
			ts := httptest.NewServer(r)
			defer ts.Close()
			resp, body := utils.TryRequest(ts, "PATCH", "/orders/test", nil, strings.NewReader(`
			{
				"status":"INVALID"
			}`))
			Expect(resp.StatusCode).To(Equal(http.StatusBadRequest))
			Expect(body).To(Equal(`{"error":"Invalid payload"}`))
		})
	})
})
