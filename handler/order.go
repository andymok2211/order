package handler

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/go-pg/pg"
	"gitlab.com/andymok221/order/maps"
	"gitlab.com/andymok221/order/model"
	"gitlab.com/andymok221/order/repository"
	"gitlab.com/andymok221/order/response"
	"gitlab.com/andymok221/order/utils"
)

type Handler struct {
	maps      maps.Map
	orderRepo repository.OrderRepository
}

type PlaceOrderRequest struct {
	Origin      []string `json:"origin"`
	Destination []string `json:"destination"`
}

func (*PlaceOrderRequest) Bind(r *http.Request) error {
	return nil
}

type TakeOrderRequest struct {
	Status string `json:"status"`
}

func (*TakeOrderRequest) Bind(r *http.Request) error {
	return nil
}

func New(orderRepo repository.OrderRepository, maps maps.Map) *Handler {
	return &Handler{
		maps:      maps,
		orderRepo: orderRepo,
	}
}

func (h *Handler) PlaceOrder(w http.ResponseWriter, r *http.Request) {
	data := &PlaceOrderRequest{}
	if err := render.Bind(r, data); err != nil {
		response.ErrResponse(w, r, http.StatusBadRequest, fmt.Errorf("Invalid payload: %s", err.Error()))
		return
	}

	if !utils.ValidateCoordinate(data.Origin) {
		response.ErrResponse(w, r, http.StatusBadRequest, errors.New("Invalid origin coordinate"))
		return
	}

	if !utils.ValidateCoordinate(data.Destination) {
		response.ErrResponse(w, r, http.StatusBadRequest, errors.New("Invalid destination coordinate"))
		return
	}

	distance, err := h.maps.GetDistance(data.Origin, data.Destination)
	if err != nil {
		response.ErrResponse(w, r, http.StatusBadRequest, errors.New("Error in getting distance"))
		return
	}

	order, err := h.orderRepo.Create(&model.Order{
		Distance: distance,
		Status:   model.StatusUnassigned,
	})
	if err != nil {
		response.ErrResponse(w, r, http.StatusInternalServerError, errors.New("Cannot create order"))
		return
	}

	response.JSONResponse(w, r, http.StatusOK, order)
}

func (h *Handler) TakeOrder(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	data := &TakeOrderRequest{}
	if err := render.Bind(r, data); err != nil {
		response.ErrResponse(w, r, http.StatusBadRequest, fmt.Errorf("Invalid payload: %s", err.Error()))
		return
	}

	if model.OrderStatus(data.Status) != model.StatusTaken {
		response.ErrResponse(w, r, http.StatusBadRequest, fmt.Errorf("Invalid payload"))
		return
	}

	_, err := h.orderRepo.Update(id, map[string]interface{}{
		"status": model.StatusTaken,
	}, map[string]interface{}{
		"status": model.StatusUnassigned,
	})
	if err != nil {
		status := http.StatusInternalServerError
		if err == pg.ErrNoRows {
			status = http.StatusBadRequest
		}
		response.ErrResponse(w, r, status, errors.New("Cannot take order"))
		return
	}

	response.JSONResponse(w, r, http.StatusOK, map[string]interface{}{"status": "SUCCESS"})
}

func (h *Handler) ListOrders(w http.ResponseWriter, r *http.Request) {
	page, err := utils.IntQuery(r, "page", 1)
	if err != nil {
		response.ErrResponse(w, r, http.StatusBadRequest, errors.New("page query should be integer"))
		return
	}

	limit, err := utils.IntQuery(r, "limit", 10)
	if err != nil {
		response.ErrResponse(w, r, http.StatusBadRequest, errors.New("limit query should be integer"))
		return
	}

	offset := (page - 1) * limit
	orders, err := h.orderRepo.Fetch(offset, limit)
	if err != nil {
		response.ErrResponse(w, r, http.StatusInternalServerError, err)
		return
	}

	response.JSONResponse(w, r, http.StatusOK, orders)
}
