.PHONY: build integration-test static-check test test-files vendor
PACKAGE_DIRS=`go list -e ./... | egrep -v "mocks|integration"`
UNIT_TEST_PACKAGE_DIRS=`go list -e ./... | egrep -v "mocks|integration"`

build:
	go build
mocks:
	./mocks.sh
integration-test:
	docker-compose up --build integration-test
static-check:
	staticcheck -checks="all","-ST1000","-ST1005" -tests=false $(PACKAGE_DIRS)
test: static-check test-files
test-files:
	go test -v $(UNIT_TEST_PACKAGE_DIRS) -race -coverprofile=c.out -covermode=atomic
vendor:
	go mod vendor