package route

import (
	"github.com/go-chi/chi"
	"gitlab.com/andymok221/order/handler"
)

func RegisterOrderRoute(r *chi.Mux, h *handler.Handler) {
	r.Post("/orders", h.PlaceOrder)
	r.Patch("/orders/{id}", h.TakeOrder)
	r.Get("/orders", h.ListOrders)
}
