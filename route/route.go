package route

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/andymok221/order/handler"
)

func New(h *handler.Handler) *chi.Mux {
	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	RegisterOrderRoute(r, h)
	return r
}
