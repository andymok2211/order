package maps

import (
	"context"
	"errors"
	"strings"

	gmapsdk "googlemaps.github.io/maps"
)

type Map interface {
	GetDistance(origin []string, destination []string) (int, error)
}

type googleMap struct {
	client *gmapsdk.Client
}

func NewGoogleMap(apiKey string) (Map, error) {
	c, err := gmapsdk.NewClient(gmapsdk.WithAPIKey(apiKey))
	if err != nil {
		return nil, err
	}

	return &googleMap{
		client: c,
	}, nil
}

func (gmap *googleMap) GetDistance(origin []string, destination []string) (int, error) {
	resp, err := gmap.client.DistanceMatrix(context.Background(), &gmapsdk.DistanceMatrixRequest{
		Origins:      []string{strings.Join(origin, ",")},
		Destinations: []string{strings.Join(destination, ",")},
		Units:        gmapsdk.UnitsMetric,
	})
	if err != nil {
		return 0, err
	}

	if len(resp.Rows) > 0 {
		row := resp.Rows[0]
		if len(row.Elements) > 0 {
			elm := row.Elements[0]
			if elm.Status == "OK" {
				return elm.Distance.Meters, nil
			}
		}
	}

	return 0, errors.New("No distance found")
}
